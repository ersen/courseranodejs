var express = require('express');
var bodyParser = require('body-parser');
var favoriteRouter = express.Router();

var mongoose = require('mongoose');

var Favorites = require('../models/favorites');
var Verify = require('./verify');

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')

.all(Verify.verifyOrdinaryUser)


.get(function (req, res, next) {
    var currentUserID = req.decoded._doc._id;
    Favorites.find({"postedBy": currentUserID})
        .populate('postedBy')
        .populate('dishes')
        .exec(function (err, favorite) {
            if (err) throw err;
            res.json(favorite);
        });
})


.post(function(req, res, next) {
  var currentUserID = req.decoded._doc._id;
  console.log("user:" + currentUserID);
  var requestedDishId = req.body._id;
  console.log("dishId:" + requestedDishId);

  Favorites.find({'postedBy': currentUserID}, function (err, favorites) {
        if (err) throw err;

        console.log("favs:" + favorites.length);

        if (favorites.length === 0) {
            var newFavorite = new Favorites({"postedBy": currentUserID, "dishes": [requestedDishId]});

            newFavorite.save(function(err, favorite) {
              if(err) throw err;

              res.json(favorite);
            });
        }
        else {
          var favorite = favorites[0];
          var dishIds = favorite.dishes;
          for (var i = dishIds.length - 1; i >= 0; i--) {
            if(dishIds[i] == requestedDishId) {
              var err = new Error('This dish already exist in favorites!');
              err.status = 400;
              return next(err);
            }
          }

          dishIds.push(requestedDishId);
          favorite.save(function(err, fav) {
            if (err) throw err;

            res.json(fav);
          });

        }
    })
})

.delete(function(req, res, next) {
  var currentUserID = req.decoded._doc._id;
  Favorites.find({'postedBy': currentUserID}, function(err, favorites) {
    if (err) throw err;

    favorites[0].remove();
    res.json(favorites);
  });

});

favoriteRouter.route('/:dishId')
.all(Verify.verifyOrdinaryUser)

.delete(function(req, res, next) {
  var currentUserID = req.decoded._doc._id;
  var requestedDishId = req.params.dishId;

  Favorites.find({'postedBy': currentUserID}, function(err, favorites) {
    if (err) throw err;

    var favorite = favorites[0];
    var dishIds = favorite.dishes;

    for (var i = dishIds.length - 1; i >= 0; i--) {
      if(dishIds[i] == requestedDishId) {
        
        dishIds.splice(i, 1);
        favorite.save(function(err, favorite) {
          if (err) throw err;

          res.json(favorite);
        });
      }
    }
    
  });

});

module.exports = favoriteRouter;